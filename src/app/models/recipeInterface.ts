export interface Recipe {
  id: number;
  title: string;
  short_description: string;
  ingredients: Ingredient[];
  description: string;
  image: string;
}

export interface Ingredient {
  name: string;
}