import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RecipeService } from 'src/app/services/recipe.service';
import { Recipe } from 'src/app/models/recipeInterface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() changeRecipe = new EventEmitter<Recipe[]>();

  constructor(
    public dialog: MatDialog,
    private recipeService: RecipeService
  ) { }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogContentAddRecipe, {
      width: '500px'
    });

    const sub = dialogRef.componentInstance.addRecipe.subscribe(() => {
      this.changeRecipe.emit(this.recipeService.getData('recipes'));
    }, (error) => {
      console.log('Error: ', error);
    });

    /*dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      this.changeRecipe.emit(this.recipeService.getData('recipes'));
    });*/
  }

}

@Component({
  selector: 'dialogs/dialog-content-add-recipe',
  templateUrl: '../../dialogs/dialog-content-add-recipe.html',
})
export class DialogContentAddRecipe {

  public formRecipe: FormGroup;
  public ingredients: any;
  public imageBase64: any = null;
  public recipes: Recipe[];

  public addRecipe = new EventEmitter<Recipe[]>();

  constructor(
    public dialogRef: MatDialogRef<DialogContentAddRecipe>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private recipeService: RecipeService
  ){
    this.formRecipe = this.formBuilder.group({
      title: ['', [Validators.required]],
      short_description: ['', [Validators.required]],
      description: ['', [Validators.required]],
      ingredients: this.formBuilder.array([
        this.newIngredient()
      ]),
      image: ['', [Validators.required]]
    });
    this.ingredients = this.formRecipe.get("ingredients") as FormArray;
  }

  newIngredient(): FormGroup {
    return this.formBuilder.group({
        name: ['', Validators.required]
    });
  }

  addIngredient(): void {
    this.ingredients.push(this.newIngredient());
  }

  removeIngredient(i: number): void {
    this.ingredients.removeAt(i);
  }

  save(): void {
    console.log("this.formRecipe: ", this.formRecipe.value)
    if(this.formRecipe.valid) {
      this.dialogRef.close();
      let recipes = this.recipeService.getData('recipes');
      this.formRecipe.value['id'] = recipes.length + 1;
      this.formRecipe.value['image'] = this.imageBase64;
      this.recipeService.addRecipe(this.formRecipe.value);
      this.addRecipe.emit();
    } else {
      alert("¡Los datos introducidos no son válidos!, Repasa los campos");
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  imageToBase64(file): void {

    const reader = new FileReader();
  
    reader.onload = (e) => {

      const imgBase64Path = e.target.result;
      this.imageBase64 = imgBase64Path;

    };

    reader.readAsDataURL(file.target.files[0]);
    
  }

  deleteImage(): void {
    this.imageBase64 = null;
  }

}
