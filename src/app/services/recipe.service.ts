import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Recipe } from 'src/app/models/recipeInterface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  public recipes: Recipe[];

  constructor(
    private http: HttpClient
  ) {
    //fetch('../assets/data/recipes.json')
    //.then(res => res.json())
    //.then(data => {
    //  console.log(data);
    //})
  }

  getRecipes(): Observable<Recipe[]> {

    let urlJson = '../assets/data/recipes.json';

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get<Recipe[]>(urlJson, {headers: headers});
  }

  setData(name, data): void {
    localStorage.setItem(name, JSON.stringify(data))
  }

  getData(name): Recipe[] {
    return JSON.parse(localStorage.getItem(name))
  }

  addRecipe(recipe: Recipe): void {
    let recipes = this.getData('recipes');
    recipes.push(recipe);
    this.setData('recipes', recipes)
  }

  removeRecipe(id: number): void {
    let recipes = this.getData('recipes');
    const iRecipe = recipes.findIndex((obj) => obj.id === id)
    if (iRecipe !== -1) recipes.splice(iRecipe, 1);
    this.setData('recipes', recipes)
  }

}
