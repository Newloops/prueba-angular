import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RecipesRoutingModule } from 'src/app/pages/recipes/recipes-routing.module';

import { HeaderComponent, DialogContentAddRecipe } from 'src/app/components/header/header.component';
import { ContentComponent } from 'src/app/components/content/content.component';
import { RecipesComponent } from 'src/app/pages/recipes/list/recipes.component';
import { RecipeDetailComponent } from 'src/app/pages/recipes/recipe-detail/recipe-detail.component';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon'
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    HeaderComponent,
    DialogContentAddRecipe,
    ContentComponent,
    RecipesComponent,
    RecipeDetailComponent
  ],
  imports: [
    CommonModule,
    RecipesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule
  ]
})
export class RecipesModule { }
