import { Component, OnInit } from '@angular/core';
//import Recipes from 'src/assets/data/recipes.json';
import { RecipeService } from 'src/app/services/recipe.service';
import { Recipe } from 'src/app/models/recipeInterface';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'] 
})
export class RecipesComponent implements OnInit {

  public recipes: Recipe[];

  constructor(
    private recipeService: RecipeService
  ) {
    this.getRecipes();
  }

  ngOnInit(): void {
  }

  getRecipes() {
    if(this.recipeService.getData('recipes') === undefined || this.recipeService.getData('recipes') === null) {
      this.recipeService.getRecipes().subscribe(res => {
        this.recipeService.setData('recipes', res);
        this.recipes = res;
      }, (error) => {
        console.log('Error: ', error);
      });
    } else {
      this.recipes = this.recipeService.getData('recipes')
    }
  }

  removeRecipe(id: number): void {
    this.recipeService.removeRecipe(id);
    this.getRecipes();
  }

}
