import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { RecipesComponent } from 'src/app/pages/recipes/list/recipes.component';

const routes: Routes = [
  {
		path: '',
    children: [
      {
        path: 'list',
        component: RecipesComponent
      },
      {
        path: ':id',
        component: RecipeDetailComponent
      },
      {
        path: '**',
        redirectTo: 'list'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipesRoutingModule { }
