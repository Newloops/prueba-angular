import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Recipe } from 'src/app/models/recipeInterface';
import { RecipeService } from 'src/app/services/recipe.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss']
})
export class RecipeDetailComponent implements OnInit {

  public idRecipe: string;
  public recipe: Recipe;

  constructor(
    private route:ActivatedRoute,
    private recipeService: RecipeService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.idRecipe = this.route.snapshot.paramMap.get('id')
    const recipes: any[] = this.recipeService.getData('recipes')
    this.recipe = recipes.find(element => element.id === parseInt(this.idRecipe));
  }

  back(): void{
    this.router.navigate(['recipes/list']);
  }

}
